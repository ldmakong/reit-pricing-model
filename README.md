# Real Estate Pricing Model 

## Requirements

- Python >=3.0
- [Anaconda](https://www.anaconda.com/)

First create a custom-named environment:

    $ conda env create -f environment.yml -n ENV_NAME

Second, update the environment to ensure all the necessary packages for the project are installed :

    $ conda env update -f environment.yml -n ENV_NAME

Third, activate the environment :

    $ source activate ENV_NAME

Finally, open jupyter to view the project notebooks :

    $ jupyter notebook 


## Goal

In this project, we used a dataset given by a Real Estate Investment Trust (REIT) to build a real estate price prediction model. The dataset contains transaction prices for previous properties on the market. Moreover, price prediction must be made with an average error of under $70,000.

## Approach

After exploring and cleaning the given dataset with key features, we used and compared different regression-based methods (Lasso, Ridge, Elastic-Net, Random Forests, Gradient Boosting) in order to find the best model to predict properties prices. 


## Results

The best pricing model is based on Random Forests since after its training its mean average error (MAE) is under $70,000. 

![model](assets/pricing_rfmodel.png)

## Author

MAKONG Ludovic / [@ldmakong](https://gitlab.com/ldmakong)


